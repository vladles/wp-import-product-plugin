<?php 


class tsImportObject {
	public $attrubute_name;

	protected $CSVDataSource;

	protected $_wcFactory;

	function __construct( $file ) { 
		$this->attrubute_name = wc_attribute_taxonomy_name( 'Seat location' );

		$this->_wcFactory = new WC_Product_Factory();
		$this->CSVDataSource = new File_CSV_DataSource();

		$this->stripBOM( $file );
		if ( !$this->CSVDataSource->load( $file ) )
			return false;

	// pad shorter rows with empty values
		$this->CSVDataSource->symmetrize();
	}


	public function import_price() {
		$rows = $this->CSVDataSource->connect();

		if ( !empty( $rows ) ) {
			global $wpdb;
			wp_defer_term_counting( true );
			wp_defer_comment_counting( true );
			$wpdb->query( 'SET autocommit = 0;' );

			foreach ( $rows as $row ) {
				if ( !empty($row['Seat Location']) && !empty($row['Price']) ) {
					$this->set_data_inItem( $row );
				}
			}

		// Reset options
			wp_defer_term_counting( false );
			wp_defer_comment_counting( false );
			$wpdb->query( 'COMMIT;' );
			$wpdb->query( 'SET autocommit = 1;' );


		}
	}

	protected function set_data_inItem( array $item ) {
		$productObject = $this->_wcFactory->get_product($item['ID']);
		// error_log( print_R($productObject, true) );
		if ( $productObject ) {
			if ( !empty($item['Seat Location']) ) {
			// Set how variable products
				wp_set_object_terms( $productObject->get_id(), 'variable', 'product_type', false );

			// Set attr.
				$product_attrubutes = explode(' | ', $item['Seat Location'] );
				wp_set_object_terms( $item['ID'], $product_attrubutes, $this->attrubute_name );
				if ( !empty($item['Price']) ) {
			// Set price for each attr.
					$product_prices = explode(' | ', $item['Price']);
					$create_variations_result = $this->create_variations( $productObject, $product_attrubutes, $product_prices );
					if ( $create_variations_result === true ){
						WC_Product_Variable::sync( $productObject->get_id() );
					// Set stock status
						update_post_meta( $productObject->get_id(), '_stock_status', 'instock' );

					}
				}
			}

		}
	}

	protected function create_variations( $productObject, $attrs, $prices ) {
	// Merge attrubute with price 
		$attr_price = array_combine($attrs, $prices);
		// error_log( print_R($attr_price, true) );
		if ( $attr_price ) {
			foreach ( $attr_price as $attr => $price ) {
				$variation_id = $this->_insert_varitation( $productObject->get_id() );
				// error_log(print_R($variation_id, true));
				if ( $variation_id ) {
				// Regular Price ( you can set other data like sku and sale price here )
					update_post_meta( $variation_id, '_regular_price', $price );
					update_post_meta( $variation_id, '_price',  $price );

				// Assign the seat location of this variation
					update_post_meta( $variation_id, 'attribute_' . $this->attrubute_name,  sanitize_title($attr) );

				// Attach image 
					// if ( !empty( $variationItem['Product Image'] ) ) {
					// 	$this->attached_post_image( $variationItem['Product Image'], $variationItem['Alt'], $variation_id );
					// }
				}
			}
			return true;
		}

		return false;
	}

	protected function _insert_varitation( $parent_id ) {
		$variation_id = wp_insert_post( array(
			'post_title'   => 'Product #' . $parent_id . ' Variation',
			'post_content' => '',
			'post_status'  => 'publish',
			'post_parent'  => $parent_id,
			'post_type'    => 'product_variation'
		));

		if( $variation_id && !is_wp_error($variation_id) )
			return $variation_id;

		return false;
	}



	/**
	 * Delete BOM from UTF-8 file.
	 *
	 * @param string $fname
	 * @return void
	 */
	private function stripBOM( $fname ) {
		$res = fopen($fname, 'rb');
		if ( false !== $res ) {
			$bytes = fread($res, 3);
			if ($bytes == pack('CCC', 0xef, 0xbb, 0xbf)) {
				$this->log('Getting rid of byte order mark...' );
				fclose($res);

				$contents = file_get_contents($fname);
				if (false === $contents) {
					wp_die( 'Failed to get file contents.' );
				}
				$contents = substr($contents, 3);
				$success = file_put_contents($fname, $contents);
				if (false === $success) {
					wp_die( 'Failed to put file contents.' );
				}
			} else {
				fclose($res);
			}
		} else {
			$this->log( 'Failed to open file, aborting.' );
		}
	}


	/**
	 * Write log file
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function log( $data ) {
		if ( empty( $data ) )
			return 0;
	// Get default dir
		$upload_dir = wp_upload_dir();
	//Something to write to txt log
		$log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
				"Message: ". $data .PHP_EOL.
				"-------------------------".PHP_EOL;
	//Save string to log, use FILE_APPEND to append.
		$filename = $upload_dir['basedir'] . '/importCSV/log_' . date("j.n.Y").'.log';
		if ( file_exists( $filename ) ) {
			file_put_contents( $filename, $log, FILE_APPEND );
		}else{ 
			file_put_contents( $filename, $log );
		}
	}


}