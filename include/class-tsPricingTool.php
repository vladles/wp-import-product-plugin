<?php



/**
* 
*/
class tsPricingTool {
	protected $csvHeader;
	protected $filepath;
	protected $attrubute_name;
	protected $_wc_factory;

	function __construct(){
		$this->attrubute_name = wc_attribute_taxonomy_name( 'Seat location' );
		$this->_wc_factory = new WC_Product_Factory();
		$filename = 'ts-pricingList-'.date('Ymd-His').'.csv';
		$this->csvHeader = array( 'ID', 'Name', 'Seat Location', 'Price' );
		$this->filepath = TSIMPORT_DIR. $filename;
		$this->fileurl  = TSIMPORT_DIRURL. $filename;
	}
	protected function addHeader($file) {
		fputcsv($file, $this->csvHeader);
		return $file;
	}

	public function get_items_by_artist($artist_id) {
		$query = new WP_Query(array(
			'post_type'			=> 'product',
			'posts_per_page'	=> -1,
			'fields'			=> 'ids',
			'tax_query'	=> array(
				array(
					'taxonomy'	=> 'artists',
					'field'		=> 'term_id',
					'terms'		=> $artist_id
				)
			)
		));
		if ( $query->have_posts() )
			return $query->posts;

	}

	public function get_items_withoutprice() {
		// $query = new WP_Query(array(
		// 	'post_type' => 'product',
		// 	'posts_per_page'	=> -1,
		// 	''
		// ));
	}



	public function create_items_array( array $lastImport_ids ) {
		$items = array();
		if ( !empty($lastImport_ids) ) {
			foreach ($lastImport_ids as $id ) {
				$productObject = $this->_wc_factory->get_product($id);
				if ( $productObject ) {
					$item = array(
						'ID' => $id,
						'Name'	=> get_the_title( $id ),
					);
					if ( $productObject->is_type('variable') ) {
						$variations = $this->get_productVariations( $productObject );
						if ( !empty($variations) ) {
							$item['Seat Location'] = implode(' | ', array_keys($variations));
							$item['Price'] = implode(' | ', array_values($variations));
						}
					}else {
						if ( $attribute = wc_get_product_terms( $id, 'pa_seat-location', array( 'fields' => 'slugs' ) ) ) {
							$item['Seat Location'] = implode(' | ', $attribute);
						}
					}
					$items[] = $item;
				}
			}
		}
		return $items;
	}

	protected function get_productVariations( $productObject ) {
		$variations = array();
		if ( $productVariations = $productObject->get_available_variations() ) {
			foreach ( $productVariations as $variable ) {
				$attr = $variable['attributes']['attribute_pa_seat-location'];
				$variations[$attr] = $variable['display_price'];
			}
		}

		return $variations;
	}

	public function createfile_with( array $items ) {
		if ( $items ) {
			$csvFile = fopen($this->filepath, 'w');
			if ( $csvFile ) {
				$csvFile = $this->addHeader($csvFile);
				foreach ( $items as $item ) {
					fputcsv( $csvFile, $item, ',' );
				}
				fclose($csvFile);
				return $this->fileurl;
			}
		}

		return false;
	}
}